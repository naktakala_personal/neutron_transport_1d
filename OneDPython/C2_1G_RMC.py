import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import math
import time
import sys

sys.path.append('./SnPWLD/')
sys.path.append('./NuclearMaterials/')
sys.path.append('./MultiGroup1DMonteCarlo/')
sys.path.append('./MeshLib/')

import MeshLib
import NuclearMaterialsLib
import GroupStructsLib
import PWLD
import AnalyticalSolutions as AnaSol
import MeshLib
import MG1DMC

import GolubFischer



Nel=20
mesh = MeshLib.OneD_Mesh(0,4,Nel)   #Mesh
mcmesh = MeshLib.OneD_Mesh(0,4,Nel) #Mesh for Monte-Carlo tallies
L = 0                               #Scat order
N_a = 2                            #Number of angles

# ========================================== Define group structure
group_struct = GroupStructsLib.MakeSimple(1)
G = np.size(group_struct)           #Number of groups
print("Number of groups read from data file: %d" %G)


# ========================================== Define materials
materials = []
materials2= []

sigma_t = 1.0
cratio = 0.0
m_testmaterial = NuclearMaterialsLib.NuclearMaterial1GIsotropic("Test1G",sigma_t,cratio)
materials.append(m_testmaterial)

#=========================================== Define BCs
bcs = []
bcs.append(np.array([PWLD.ISOTROPIC,PWLD.ISOTROPIC])) #Boundary type ident
bcs.append(np.zeros(G))                            #Left boundary groupwise values
bcs.append(np.zeros(G))                            #Right boundary groupwise values

bcs[1][0] = 1.0
source = np.zeros((G,Nel))+0.0

#=========================================== Define Groupsets
groupsets = []
groupsets.append(np.array([0, 0, PWLD.WITH_DSA, 20, 150]))

#=========================================== Transport solve
snsolver = PWLD.PWLD(L,G,N_a,mesh,materials,source,bcs)
snsolver.Initialize()
snsolver.SetOutputFileName("ZOut_B0.txt")
snsolver.Solve(groupsets,group_struct)

snsolver.ComputeResidual(0,0)
snsolver.WriteRestartData()


#=========================================== MonteCarlo solve
num_particles = int(6000*40)
batch_size    = int(6000)
num_threads = 6
mcsource = []
mcsolver = MG1DMC.MultiGrp1DMC(L,G,mcmesh,materials,mcsource,bcs,group_struct,num_threads)
mcsolver.SourceRoutine = mcsolver.LeftIsotropicFlux #DistributedIsotropic #LeftIsotropicFlux #mcsolver.CustomSource

mcsolver.RunMTForward(num_particles,batch_size,False)




# ===================================== PLOT FORWARD SOLUTIONS
plt.figure(0)
plt.clf()

x   , phi        = snsolver.GetSnPhi_g(0)  #Discontinuous
x2  , phi2, err2 = mcsolver.GetPhi_g(0)
xavg, phi_avg    = snsolver.GetAvgSnPhi_g(0)

plt.plot(x   , phi  ,"g", label=r'$S_{'+str(N_a)+ '}$ Solution $N_{el}$='+str(Nel))
plt.plot(x2  , phi2 ,"-", label='Monte-Carlo')


plt.xlim([0,4])
plt.title(r'Forward ')
plt.xlabel('Mean free paths')
plt.ylabel('$\phi$',rotation=0)
plt.legend()
plt.grid(which='major')
plt.yscale('log')
#plt.savefig("/Users/janv4/Desktop/Personal/RMCReport/Figure4B.png",dpi=600)
plt.show()
#exit(0)




#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#                   ERROR ANALYSIS
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#=========================================== MOC solve
num_particles = int(6000*40/40)
batch_size    = int(6000)
num_threads = 6
mocsource = []
mocsolver = MG1DMC.MultiGrp1DMC(L,G,mcmesh,materials,mcsource,bcs,group_struct,num_threads)

xn  , phin       = snsolver.ComputeNodeAverages()
mocsolver.TraceUncollidedCharacteristicsResid(20,25,phin)
mocsolver.moc_flux_tot = np.sum(mocsolver.MOCflux)
mocsolver.SourceRoutine = mocsolver.TracedSource
mocsolver.RunMTForward(num_particles,batch_size,False)

x3  , phi3, err3 = mocsolver.GetPhi_g(0)
phi3+= mocsolver.MOCflux

delta_phi_true = phi_avg - phi2

# ====== Plot
plt.figure(1)
plt.clf()

plt.plot(xavg,delta_phi_true,'g',label=r'$\Delta \phi_{true}$')
plt.plot(x3,-phi3/40,'k',label=r'$\Delta \tilde{\phi}$')

plt.xlim([0,4])
plt.title(r'Error ')
plt.xlabel('Mean free paths')
plt.ylabel('$\Delta \phi$',rotation=0)
plt.legend()
plt.grid(which='major')
#plt.yscale('log')
#plt.savefig("/Users/janv4/Desktop/Personal/RMCReport/Figure4B.png",dpi=600)
plt.show()