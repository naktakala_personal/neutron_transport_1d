import numpy as np
import math

X = 0
Y = 1
Z = 2

class MG1DMC_Methods:
  def TraceUncollidedCharacteristics(self, num_ang, sub_res):
    self.moc_fine_res = self.mesh.Ndiv*sub_res+1
    self.moc_dx       = self.mesh.h/sub_res

    self.traces = np.zeros((num_ang,self.moc_fine_res))
    self.tphi=np.zeros((self.moc_fine_res))

    mu=np.linspace(-0.999999,0.999999,num_ang)
    dmu=2/num_ang

    for n in range(0,num_ang):
      if mu[n]>0.0:
        x=self.mesh.xmin
        s=(x-self.mesh.xmin)/mu[n]
        j=0
        self.traces[n,j] = 1.0/4.0/math.pi
        self.tphi[j] += 2*math.pi*self.traces[n,j]*dmu


        for k in range(0,self.mesh.Ndiv):
          elem_k = self.mesh.elements[k]
          mat    = self.materials[elem_k.mat_id]

          for i in range(0,sub_res):
            x=elem_k.xi__ + self.moc_dx/2 + i*self.moc_dx
            s=(x-self.mesh.xmin)/mu[n]
            j+=1

            self.traces[n,j] = 2*math.pi*self.traces[n,0]*math.exp(-s*mat.sigma_t[0])*dmu
            self.tphi[j]+=self.traces[n,j]

            self.MOCflux[k]+=self.traces[n,j]*(self.moc_dx/elem_k.h)
      else:
        x=self.mesh.xmax
        s=(x-self.mesh.xmax)/mu[n]
        j=self.moc_fine_res-1
        self.traces[n,j]=0.0
        self.tphi[j]+=2*math.pi*self.traces[n,j]*dmu

        for k in range(self.mesh.Ndiv-1,-1,-1):
          elem_k=self.mesh.elements[k]
          mat=self.materials[elem_k.mat_id]

          for i in range(0,sub_res):
            x=elem_k.xip1-self.moc_dx/2-i*self.moc_dx
            s=(x-self.mesh.xmax)/mu[n]
            j-=1

            jmax = self.moc_fine_res-1
            self.traces[n,j]=2*math.pi*self.traces[n,jmax]*math.exp(-s*mat.sigma_t[0])*dmu
            self.tphi[j]+=self.traces[n,j]

            self.MOCflux[k]+=self.traces[n,j]*(self.moc_dx/elem_k.h)

  def R_mu_k_x(self,mu,k,x,phi):
    elem_k = self.mesh.elements[k]
    mat = self.materials[elem_k.mat_id]
    phiL = phi[k]
    phiR = phi[k+1]

    dphidx = (phiR-phiL)/elem_k.h

    phi_x  = phiR*(x - elem_k.xi__)/elem_k.h
    phi_x += phiL*(elem_k.xip1 - x)/elem_k.h

    q = 0.0
    N = 1.0/4.0/math.pi

    R  = q/N
    R += mat.sigma_s[0]*phi_x/N - mat.sigma_t[0]*phi_x/N
    R += -mu*dphidx/N

    return R

  def TraceUncollidedCharacteristicsResid(self, num_ang, sub_res,phi):
    self.moc_fine_res = self.mesh.Ndiv*sub_res+1
    self.moc_dx       = self.mesh.h/sub_res

    self.traces = np.zeros((num_ang,self.moc_fine_res))
    self.tphi=np.zeros((self.moc_fine_res))

    mu=np.linspace(-0.999999,0.999999,num_ang)
    dmu=2/num_ang

    for n in range(0,num_ang):
      if mu[n]>0.0:
        x=self.mesh.xmin
        j=0
        self.traces[n,j] = -(1.0/4.0/math.pi-phi[0])/4.0
        self.tphi[j] += 2*math.pi*self.traces[n,j]*dmu

        phi0=self.traces[n,j]

        for k in range(0,self.mesh.Ndiv):
          elem_k = self.mesh.elements[k]
          mat    = self.materials[elem_k.mat_id]

          for i in range(0,sub_res):
            x=elem_k.xi__ + self.moc_dx/2 + i*self.moc_dx
            j+=1


            S = phi0*math.exp(-(self.moc_dx)*mat.sigma_t[0]/mu[n])
            # S+= self.R_mu_k_x(mu[n],k,x,phi)

            for jr in range(0,i+1):
              xj = elem_k.xi__+self.moc_dx/2+jr*self.moc_dx
              Q  = self.R_mu_k_x(mu[n],k,xj,phi)
              S += Q*math.exp(-(x-xj)*mat.sigma_t[0]/mu[n])*self.moc_dx/mu[n]

            self.traces[n,j] = 2*math.pi*S*dmu
            self.tphi[j]+=self.traces[n,j]

            self.MOCflux[k]+=self.traces[n,j]*(self.moc_dx/elem_k.h)

            if i==(sub_res-1):
              phi0=S
      else:
        x=self.mesh.xmax
        j=self.moc_fine_res-1
        self.traces[n,j]=(phi[self.mesh.Ndiv] - 0.0)/4.0
        self.tphi[j]+=2*math.pi*self.traces[n,j]*dmu

        phi0=self.traces[n,j]

        for k in range(self.mesh.Ndiv-1,-1,-1):
          elem_k=self.mesh.elements[k]
          mat=self.materials[elem_k.mat_id]



          for i in range(0,sub_res):
            x=elem_k.xip1-self.moc_dx/2-i*self.moc_dx
            j-=1


            S = phi0*math.exp(-(x - elem_k.xip1)*mat.sigma_t[0]/mu[n])

            for jr in range(0,i+1):
              xj = elem_k.xip1-self.moc_dx/2-jr*self.moc_dx
              Q  = self.R_mu_k_x(mu[n],k,xj,phi)
              S += Q*math.exp(-(x-xj)*mat.sigma_t[0]/mu[n])*self.moc_dx/abs(mu[n])

            self.traces[n,j]=2*math.pi*S*dmu*self.moc_dx
            self.tphi[j]+=self.traces[n,j]

            #self.MOCflux[k]+=self.traces[n,j]*(self.moc_dx/elem_k.h)

            if i==(sub_res-1):
              phi0=S