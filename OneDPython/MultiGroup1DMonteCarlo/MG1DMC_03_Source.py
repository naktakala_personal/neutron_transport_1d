import numpy as np
import math

import MG1DMC_04_MOC

X = 0
Y = 1
Z = 2

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
class Particle:
  def __init__(self,Egrp,omega,pos=np.zeros(3),index=-1):
    self.Egrp = Egrp
    self.omega = omega
    self.pos = pos
    self.weight = 1.0
    self.index = index

    self.alive=True
    self.cur_matid=0

    self.rmc_sample_weight = 1.0



class MG1DMC_Methods(MG1DMC_04_MOC.MG1DMC_Methods):
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Get source particle
  def GetSourceParticle(self,index):
    # ================================= Determine random angle direction
    theta = math.acos(self.rngen.RN()*2.0-1.0)
    vaphi = self.rngen.RN()*2.0*math.pi
    omega = np.zeros(3)
    omega[X] = math.sin(theta)*math.cos(vaphi)
    omega[Y] = math.sin(theta)*math.sin(vaphi)
    omega[Z] = math.cos(theta)

    newParticle = Particle(0,omega,np.array([0.0,0.0,-0.001]),index)

    if (omega[Z]>0.0):
      newParticle.weight = 1.0*omega[Z]

    # zmax = 0.125
    # #zmax = 0.001
    # newParticle=Particle(0, omega, np.array([0.0, 0.0, self.rngen.RN()*zmax]))
    # newParticle.weight = 1.0*zmax

    return newParticle

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Left surface
  def LeftIsotropicFlux(self,index, additional_w=1.0):
    # ================================= Determine random angle direction
    # mu = math.sqrt(self.rngen.RN())
    # theta = math.acos(mu)
    theta = math.acos(self.rngen.RN()*2.0-1.0)
    # theta = math.pi*self.rngen.RN()
    vaphi = self.rngen.RN()*2.0*math.pi
    omega = np.zeros(3)
    omega[X] = math.sin(theta)*math.cos(vaphi)
    omega[Y] = math.sin(theta)*math.sin(vaphi)
    omega[Z] = math.cos(theta)

    z = self.mesh.xmin-self.epsilon
    newParticle = Particle(0,omega,np.array([0.0,0.0,z]),index)

    newParticle.weight=1.0*math.cos(theta)*additional_w

    return newParticle

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Right surface
  def RightIsotropicFlux(self,index, additional_w=1.0):
    # ================================= Determine random angle direction
    theta = math.acos(self.rngen.RN()*2.0-1.0)
    vaphi = self.rngen.RN()*2.0*math.pi
    omega = np.zeros(3)
    omega[X] = math.sin(theta)*math.cos(vaphi)
    omega[Y] = math.sin(theta)*math.sin(vaphi)
    omega[Z] = math.cos(theta)

    z = self.mesh.xmax-self.epsilon
    newParticle = Particle(0,omega,np.array([0.0,0.0,z]),index)

    newParticle.weight=1.0*abs(math.cos(theta))*additional_w

    return newParticle

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Distributed source
  def DistributedIsotropic(self, index, additional_w=1.0):
    # ================================= Determine random angle direction
    theta=math.acos(self.rngen.RN()*2.0-1.0)
    vaphi=self.rngen.RN()*2.0*math.pi
    omega=np.zeros(3)
    omega[X]=math.sin(theta)*math.cos(vaphi)
    omega[Y]=math.sin(theta)*math.sin(vaphi)
    omega[Z]=math.cos(theta)

    z=self.mesh.xmin + self.rngen.RN()*(self.mesh.xmax - self.mesh.xmin)
    newParticle=Particle(0, omega, np.array([0.0, 0.0, z]), index)

    newParticle.weight=(self.mesh.xmax - self.mesh.xmin)*additional_w

    return newParticle

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Combine source
  def CombinationA(self,index):
    xi = self.rngen.RN()
    newParticle = []
    if (xi<0.33333333):
      newParticle = self.LeftIsotropicFlux(index,2.5/0.333333)
    elif (xi<0.666666):
      newParticle = self.RightIsotropicFlux(index,2.5/0.333333)
    else:
      newParticle = self.DistributedIsotropic(index,1/0.333333)

    return newParticle

  # ========================================== Monte-Carlo Solve
  def CustomSource(self, index):
    # ================================= Determine random angle direction
    theta=math.acos(self.rngen.RN()*2.0-1.0)
    vaphi=self.rngen.RN()*2.0*math.pi
    omega=np.zeros(3)
    omega[X]=math.sin(theta)*math.cos(vaphi)
    omega[Y]=math.sin(theta)*math.sin(vaphi)
    omega[Z]=math.cos(theta)

    z=self.mesh.xmin+self.rngen.RN()*5.0
    newParticle=Particle(0, omega, np.array([0.0, 0.0, z]), index)

    newParticle.weight=5.0

    return newParticle


  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RMC source
  def RMCSource(self,index):

    rn = math.ceil(self.rngen.RN()*float(self.RMC_mesh.Ndiv+1))
    #print(rn)
    self.InitializeRMC(self.RMC_mesh,rn-1,0)

    elem_k=self.RMC_cur_elem

    # ============================ Determine location
    sampleLeft = False
    sampleRite = False
    sampleCent = False
    center_res = abs(elem_k.residual_int_g[0])
    surfL_res  = abs(elem_k.residual_s_0_g[0])
    surfR_res  = abs(elem_k.residual_s_1_g[0])

    # if (self.RMC_cur_elem_num==0):
    #   surfL_res=abs(elem_k.residual_s_0_g[0])*0.5
    #
    # if (self.RMC_cur_elem_num==(self.RMC_mesh.Ndiv-1)):
    #   surfR_res=abs(elem_k.residual_s_1_g[0])*0.5

    if (self.RMC_cur_elem_num < (self.RMC_mesh.Ndiv)):
      cell_R=center_res+surfL_res
      rn=self.rngen.RN()
      if (rn<(center_res/cell_R)):
        sampleCent=True
      else:
        sampleLeft=True
    else:
      cell_R=surfR_res
      sampleRite=True
    #cell_R=center_res+surfL_res

    # rn = self.rngen.RN()
    # if (rn<(center_res/cell_R)):
    #   sampleCent=True
    # elif (rn<((center_res+surfL_res)/cell_R)):
    #   sampleLeft = True
    # else:
    #   sampleRite = True


    # ============================ Sample left
    if sampleLeft:
      theta=math.acos(self.rngen.RN()*2.0-1.0)
      vaphi=self.rngen.RN()*2.0*math.pi
      omega=np.zeros(3)
      omega[X]=math.sin(theta)*math.cos(vaphi)
      omega[Y]=math.sin(theta)*math.sin(vaphi)
      omega[Z]=math.cos(theta)

      z=self.RMC_cur_elem.xi__ +self.epsilon
      newParticle=Particle(0, omega, np.array([0.0, 0.0, z]), index)

      newParticle.weight=(cell_R/self.RMC_res_tot)* \
                         self.RMC_cur_elem.residual_s_0_g[0]/abs(self.RMC_cur_elem.residual_s_0_g[0])* \
                         (math.cos(theta))

    # ============================ Sample Rite
    if sampleRite:
      theta=math.acos(self.rngen.RN()*2.0-1.0)
      vaphi=self.rngen.RN()*2.0*math.pi
      omega=np.zeros(3)
      omega[X]=math.sin(theta)*math.cos(vaphi)
      omega[Y]=math.sin(theta)*math.sin(vaphi)
      omega[Z]=math.cos(theta)

      z=self.RMC_cur_elem.xip1 - self.epsilon
      newParticle=Particle(0, omega, np.array([0.0, 0.0, z]), index)

      newParticle.weight=(cell_R/self.RMC_res_tot)* \
                         self.RMC_cur_elem.residual_s_1_g[0]/abs(self.RMC_cur_elem.residual_s_1_g[0])* \
                         (math.cos(theta))



    # ============================ Sample Center
    if sampleCent:
      theta=math.acos(self.rngen.RN()*2.0-1.0)
      vaphi=self.rngen.RN()*2.0*math.pi
      omega=np.zeros(3)
      omega[X]=math.sin(theta)*math.cos(vaphi)
      omega[Y]=math.sin(theta)*math.sin(vaphi)
      omega[Z]=math.cos(theta)

      z=self.RMC_cur_elem.xi__ + self.rngen.RN()*self.RMC_cur_elem.h
      newParticle=Particle(0, omega, np.array([0.0, 0.0, z]), index)

      newParticle.weight=(cell_R/self.RMC_res_tot)* \
                         self.RMC_cur_elem.residual_int_g[0]/abs(self.RMC_cur_elem.residual_int_g[0])


    #newParticle.rmc_sample_weight = self.RMC_cur_elem.h

    return newParticle

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Distributed source
  def BoundaryMOC(self, index, additional_w=1.0):
    # ================================= Determine left or right
    rn=self.rngen.RN()
    sampleLeft = False
    sampleRite = False
    if rn<0.5:
      sampleLeft = True
    else:
      sampleRite = True



    # ================================= Determine random angle direction
    theta = 0.0
    if sampleRite:
      theta=math.acos(self.rngen.RN())
    else:
      theta=math.acos(-self.rngen.RN())
    vaphi=self.rngen.RN()*2.0*math.pi
    omega=np.zeros(3)
    omega[X]=math.sin(theta)*math.cos(vaphi)
    omega[Y]=math.sin(theta)*math.sin(vaphi)
    omega[Z]=math.cos(theta)

    mu = omega[Z]
    if (mu<1.0e-4):
      mu = 1.0e-4

    elem_k = []
    z = 0.0
    additional_w = 0.0
    if sampleLeft:
      elem_k = self.RMC_mesh.elements[0]
      z = elem_k.xi__+1.0e-8
      additional_w=elem_k.residual_s_0_g[0]
    else:
      elem_k = self.RMC_mesh.elements[self.RMC_mesh.Ndiv-1]
      z = elem_k.xip1-1.0e-8
      additional_w=elem_k.residual_s_1_g[0]

    newParticle=Particle(0, omega, np.array([0.0, 0.0, z]), index)


    newParticle.weight=1.0*math.cos(theta)*additional_w

    return newParticle

  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Left surface
  def ParticleAtLocation(self,index,additional_w=1.0,pos=np.array([0.0,0.0,0.0])):
    # ================================= Determine random angle direction
    # mu = math.sqrt(self.rngen.RN())
    # theta = math.acos(mu)
    theta=math.acos(self.rngen.RN()*2.0-1.0)
    vaphi=self.rngen.RN()*2.0*math.pi
    omega=np.zeros(3)
    omega[X]=math.sin(theta)*math.cos(vaphi)
    omega[Y]=math.sin(theta)*math.sin(vaphi)
    omega[Z]=math.cos(theta)

    newParticle=Particle(0,omega,pos,index)

    newParticle.weight=additional_w

    return newParticle
