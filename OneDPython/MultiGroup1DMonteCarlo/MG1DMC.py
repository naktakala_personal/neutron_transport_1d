import numpy as np
import math
import multiprocessing
import time
import sys
import queue

import MG1DMC_01_Utilities


VACUUM = 0
ISOTROPIC = 1
REFLECTIVE = 2
LEFT = 0
RIGHT = 1

X = 0
Y = 1
Z = 2



# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
class RandomNumberGenerator:
  def __init__(self):
    self.RNGen = np.random.RandomState()
    self.total_calls = 0
    self.cumulative = 0.0

  def RN(self):
    self.total_calls += 1
    numb = self.RNGen.uniform(0.0,1.0)
    self.cumulative += numb

    return numb

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
class Tally:
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Constructor
  # batch_size: The amount of contributions to
  # assimilate before making an estimation of the
  # mean.
  def __init__(self,keep_history=False):
    self.keep_history = keep_history
    self.hist_initialized = False
    self.history_mean = []

    self.v_sx = 0.0                    # Accumulator of tally value
    self.v_sx2= 0.0                    # Accumulator of tally value squared
    self.v_sx_contrib_count = 0        # Counts contributions from unique indexes
    self.last_index = -1

    self.mu = 0.0                      # Running average
    self.mu_sx = 0.0                   # Accumulator of tally mean value
    self.mu_sx2 = 0.0                  # Accumulator of tally mean squared value
    self.mu_sx_contrib_count=0         # Counts contributions to mean value


  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Contribute
  def Contrib(self,value,contrib_index=-1):
    self.v_sx    += value
    self.v_sx2   += (value*value)

    if contrib_index!=self.last_index:
      self.v_sx_contrib_count = contrib_index
      self.last_index = contrib_index

  def ContribMean(self):
    mean = self.Mean();
    self.mu_sx+=mean
    self.mu_sx2+=mean*mean
    self.mu_sx_contrib_count+=1

    if (self.keep_history):
      self.history_mean.append(mean)


  def StdDev(self):
    b = self.mu_sx_contrib_count
    binv = 1.0/b

    Sx = self.mu_sx*binv
    Sx2= self.mu_sx2

    # return math.sqrt(binv*(Sx2 - binv*Sx*Sx))
    return math.sqrt(b/(b-1))*math.sqrt(binv*Sx2 - Sx*Sx)

  def StdDevTemp(self):
    b = self.v_sx_contrib_count
    binv = 1.0/b

    Sx = self.v_sx*binv
    Sx2= self.v_sx2
    print("Sx,sx2,b = %g %g %g" %(Sx,Sx2,b))

    # return math.sqrt(binv*(Sx2 - binv*Sx*Sx))
    return math.sqrt(binv*(binv*Sx2 - (Sx*Sx)))

  def MeanMu(self):
    if (self.mu_sx_contrib_count==0):
      binv = 1.0
    else:
      binv = 1.0/self.mu_sx_contrib_count

    return binv*self.mu_sx

  def Mean(self):
    if (self.v_sx_contrib_count==0):
      binv = 1.0
    else:
      binv = 1.0/self.v_sx_contrib_count

    return binv*self.v_sx

  def ZeroMu(self):
    self.mu_sx = 0.0
    self.mu_sx2= 0.0
    self.mu_sx_contrib_count = 0
    self.history_mean = []



# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
class MultiGrp1DMC(MG1DMC_01_Utilities.MG1DMC_Methods):
  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Constructor
  def __init__(self,L,G,mesh,materials,source,bcs,group_struct,num_threads=1):
    self.L=L
    self.G=G
    self.mesh=mesh
    self.materials=materials
    self.source=source
    self.bcs=bcs
    self.group_struct = group_struct

    self.epsilon = 1.0e-8

    self.num_threads = num_threads

    self.avg_mu = 0.0
    self.cumul_mu = 0.0
    self.mu_calls = 0

    self.rngen = RandomNumberGenerator()

    self.SourceRoutine = self.GetSourceParticle
    self.Raytracer = self.RayTrace
    self.TallyContributor = self.ContribTallies

    self.RMC_active = False
    self.RMC_mesh = []
    self.RMC_cur_elem = []
    self.RMC_cur_elem_num = -1
    self.RMC_element_CDF = []
    self.RMC_jumps = []
    self.RMC_avg_flux = []
    self.RMC_res_tot = 0.0
    self.RMC_res_tot_set = False
    self.RMC_CRF = []

    self.bank = []
    self.banked_Particles=[]

    self.reflectLeft = False
    self.reflectRite = False

    self.moc_fine_res = []
    self.moc_dx       = []
    self.moc_num_ang  = []
    self.moc_sub_res  = []
    self.moc_flux_tot = []
    self.moc_mu       = []
    self.traces = []
    self.tracex =[]



    # Default tallies for each process
    # These are sums
    self.glob_tally = np.zeros(self.G)
    self.elem_tally = np.zeros((self.G,self.mesh.Ndiv))

    self.elem_tally2=[]
    self.elem_tally2_pos=[]
    self.elem_tally2_neg=[]
    for g in range(0,self.G):
      mesh_tally=[]
      mesh_tally_pos=[]
      mesh_tally_neg=[]
      for k in range(0, self.mesh.Ndiv):
        mesh_tally.append(Tally())
        mesh_tally_pos.append(Tally())
        mesh_tally_neg.append(Tally())
      self.elem_tally2.append(mesh_tally)
      self.elem_tally2_pos.append(mesh_tally_pos)
      self.elem_tally2_neg.append(mesh_tally_neg)



    # Combined tallies across processes
    self.comb_glob_tally=np.zeros(self.G)
    self.comb_elem_tally=np.zeros((self.G, self.mesh.Ndiv))

    self.comb_elem_tally2=[]
    self.comb_elem_tally2_pos=[]
    self.comb_elem_tally2_neg=[]
    for g in range(0, self.G):
      mesh_tally=[]
      mesh_tally_pos=[]
      mesh_tally_neg=[]
      for k in range(0, self.mesh.Ndiv):
        mesh_tally.append(Tally())
        mesh_tally_pos.append(Tally())
        mesh_tally_neg.append(Tally())
      self.comb_elem_tally2.append(mesh_tally)
      self.comb_elem_tally2_pos.append(mesh_tally_pos)
      self.comb_elem_tally2_neg.append(mesh_tally_neg)



    self.mesh.elements[0].xi__ -= self.epsilon
    self.mesh.elements[self.mesh.Ndiv-1].xip1 +=self.epsilon

    self.outputFileName=""
    self.outputFileNameSet=False

    self.particles_ran=0
    self.processQ = []     #Process que to share tallies
    self.threads=[]

    self.num_batches = 0


  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Combine tallies
  def CombineTallies(self,process_que):
    for thd in range(0,self.num_threads):
      #que = process_que[thd].get()
      que = self.unifiedQ[thd]

      que_glob_tally=que[0]
      for g in range(0, self.G):
        self.comb_glob_tally[g] += que_glob_tally[g]

      que_elem_tally=que[1]
      for g in range(0, self.G):
        for k in range(0,self.mesh.Ndiv):
          self.comb_elem_tally[g,k] += que_elem_tally[g,k]

      que_elem_tally2=que[2]
      que_elem_tally2_pos=que[3]
      que_elem_tally2_neg=que[4]
      for g in range(0, self.G):
        for k in range(0,self.mesh.Ndiv):
          self.comb_elem_tally2[g][k].v_sx += \
            que_elem_tally2[g][k].v_sx
          self.comb_elem_tally2[g][k].v_sx2+= \
            que_elem_tally2[g][k].v_sx2
          self.comb_elem_tally2[g][k].v_sx_contrib_count+= \
            que_elem_tally2[g][k].v_sx_contrib_count

          self.comb_elem_tally2[g][k].ContribMean()

          #Pos
          self.comb_elem_tally2_pos[g][k].v_sx+= \
            que_elem_tally2_pos[g][k].v_sx
          self.comb_elem_tally2_pos[g][k].v_sx2+= \
            que_elem_tally2_pos[g][k].v_sx2
          self.comb_elem_tally2_pos[g][k].v_sx_contrib_count+= \
            que_elem_tally2_pos[g][k].v_sx_contrib_count

          self.comb_elem_tally2_pos[g][k].ContribMean()

          #Neg
          self.comb_elem_tally2_neg[g][k].v_sx+= \
            que_elem_tally2_neg[g][k].v_sx
          self.comb_elem_tally2_neg[g][k].v_sx2+= \
            que_elem_tally2_neg[g][k].v_sx2
          self.comb_elem_tally2_neg[g][k].v_sx_contrib_count+= \
            que_elem_tally2_neg[g][k].v_sx_contrib_count

          self.comb_elem_tally2_neg[g][k].ContribMean()


  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Run the forward problem
  def RunMTForward(self,num_particles,batch_size,report_intvl=10000,verbose=False):
    print("Running MC solver (forward)")
    particles_per_thread = int(batch_size/self.num_threads)
    self.num_batches = int(num_particles/batch_size)

    t_cycle=time.time()
    print("Number of particles total %d"%num_particles)
    print("Batch size %d"%batch_size)
    print("Number of particles per thread %d"%particles_per_thread)
    b_cycle = 0
    for b in range(0,self.num_batches):

      t_cycle = time.time()

      self.threads=[]
      self.processQ = []
      for th in range(0, self.num_threads):
        self.processQ.append(multiprocessing.Queue())
        self.threads.append(multiprocessing.Process(target=self.WorkFunction,
                            args=(particles_per_thread, th, self.processQ[th])))
        self.threads[th].start()

      self.unifiedQ = []

      #print("Joining threads")
      for th in range(0, self.num_threads):
        self.unifiedQ.append(self.processQ[th].get())
        self.threads[th].join()
        #print("Joined thread %d" %th)


      b_cycle+=1
      if (b_cycle>10):
        self.ZeroMuTallies()
        b_cycle=0
      self.particles_ran += batch_size
      #print("Combining tallies")
      self.CombineTallies(self.processQ)

      t_cycle=time.time()-t_cycle
      perf=batch_size/t_cycle
      print("Particles=%7d, rate=%.0f part/sec "%(self.particles_ran, perf))

    self.WriteRestartData()

    if self.Raytracer == self.RayTrace_MOC:
      self.bank = np.zeros(self.mesh.Ndiv)
      for k in range(0,self.mesh.Ndiv):
        self.bank[k] = self.comb_elem_tally2[0][k].Mean()
        if k>0:
          self.bank[k] += self.bank[k-1]
      print(self.bank[self.mesh.Ndiv-1])
      self.bank /= self.bank[self.mesh.Ndiv-1]
      print((self.bank))



  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Thread work function
  def WorkFunction(self,num_particles,thread_id,processQ):
    # =========================================== Initialize RN generator
    t=int(time.time()*1000.0)
    np.random.seed(thread_id + self.particles_ran)
    self.rngen = RandomNumberGenerator()

    # =========================================== Sample particles
    for p in range(0,num_particles):
      particle = self.SourceRoutine(p+1)
      while (particle.alive):
        particle = self.Raytracer(particle,thread_id)

      while (len(self.banked_Particles)>0):
        particle=self.banked_Particles.pop(0)
        while (particle.alive):
          particle=self.Raytracer(particle,thread_id)


    if self.mu_calls>0:
      self.avg_mu = self.cumul_mu/self.mu_calls

    tallies=[]
    tallies.append(self.glob_tally)
    tallies.append(self.elem_tally)
    tallies.append(self.elem_tally2)
    tallies.append(self.elem_tally2_pos)
    tallies.append(self.elem_tally2_neg)
    processQ.put(tallies)
    #print("Thread %2d finished"%thread_id)



  # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Zero-out the tallies
  def ZeroOutTallies(self):
    self.particles_ran = 0
    self.comb_glob_tally=np.zeros(self.G)
    self.comb_elem_tally=np.zeros((self.G, self.mesh.Ndiv))

    self.comb_elem_tally2=[]
    for g in range(0, self.G):
      mesh_tally=[]
      for k in range(0, self.mesh.Ndiv):
        mesh_tally.append(Tally())
      self.comb_elem_tally2.append(mesh_tally)

  def ZeroMuTallies(self):
    for g in range(0, self.G):
      mesh_tally=[]
      for k in range(0, self.mesh.Ndiv):
        self.comb_elem_tally2[g][k].ZeroMu()
        self.comb_elem_tally2_pos[g][k].ZeroMu()
        self.comb_elem_tally2_neg[g][k].ZeroMu()

