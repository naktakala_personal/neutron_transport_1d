import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import math
import time
import sys

sys.path.append('./SnPWLD/')
sys.path.append('./NuclearMaterials/')
sys.path.append('./MultiGroup1DMonteCarlo/')
sys.path.append('./MeshLib/')

import MeshLib
import NuclearMaterialsLib
import GroupStructsLib
import PWLD
import AnalyticalSolutions as AnaSol
import MeshLib
import MG1DMC

import GolubFischer



Nel=100
mesh = MeshLib.OneD_Mesh(0,4,Nel)  #Mesh
mesh2= MeshLib.OneD_Mesh(0,4,Nel)  #Mesh
mcmesh = MeshLib.OneD_Mesh(0,4,Nel) #Mesh for Monte-Carlo tallies
L = 0                               #Scat order
N_a = 2                         #Number of angles

# ========================================== Define group structure
group_struct = GroupStructsLib.MakeSimple(1)
G = np.size(group_struct)           #Number of groups
print("Number of groups read from data file: %d" %G)


# ========================================== Define materials
materials = []
materials2= []

m_testmaterial = NuclearMaterialsLib.NuclearMaterial1GIsotropic("Test1G",1.0,0.5)
m_testmaterial2 = NuclearMaterialsLib.NuclearMaterial1GIsotropic("Test1G",1.0,0.0)
materials.append(m_testmaterial)
materials2.append(m_testmaterial2)

#=========================================== Define BCs
bcs = []
bcs.append(np.array([PWLD.ISOTROPIC,PWLD.ISOTROPIC])) #Boundary type ident
bcs.append(np.zeros(G))                            #Left boundary groupwise values
bcs.append(np.zeros(G))                            #Right boundary groupwise values

bcs[1][0] = 1.0
# bcs[2][0] = 2.5
source = np.zeros((G,Nel))+0.0

# for k in range(0,20):
#   source[0,k] = 1.0

#=========================================== Define Groupsets
groupsets = []
groupsets.append(np.array([0, 0, PWLD.WITH_DSA, 20, 150]))

#=========================================== Transport solve
solver = PWLD.PWLD(L,G,N_a,mesh,materials,source,bcs)
# solver.lumped = True
solver.Initialize()
solver.SetOutputFileName("ZOut_B0.txt")
#solver.ReadRestartData("ZOut.txt")
solver.Solve(groupsets,group_struct)

for k in range(0,mesh.Ndiv):
  elem_k = mesh.elements[k]

solver.ComputeResidual(0,0)
solver.WriteRestartData()

#=========================================== Transport solve
solver2 = PWLD.PWLD(L,G,N_a,mesh2,materials2,source,bcs)
# solver.lumped = True
solver2.Initialize()
solver2.Solve(groupsets,group_struct)


#=========================================== MonteCarlo solve
num_particles = int(6000*40*16)
batch_size    = int(6000)
num_threads = 6
mcsource = []
mcsolver = MG1DMC.MultiGrp1DMC(L,G,mcmesh,materials,mcsource,bcs,group_struct,num_threads)
mcsolver.SourceRoutine = mcsolver.LeftIsotropicFlux #DistributedIsotropic #LeftIsotropicFlux #mcsolver.CustomSource

mcsolver.RunMTForward(num_particles,batch_size,False)

#=========================================== MOC solve
num_particles = int(6000*40*16)
batch_size    = int(6000)
num_threads = 6
mocsource = []
mocsolver = MG1DMC.MultiGrp1DMC(L,G,mcmesh,materials,mcsource,bcs,group_struct,num_threads)
mocsolver.SourceRoutine = mocsolver.LeftIsotropicFlux #DistributedIsotropic #LeftIsotropicFlux #mcsolver.CustomSource

mocsolver.TraceUncollidedCharacteristics(100,100)
mocsolver.moc_flux_tot = np.sum(mocsolver.MOCflux)
mocsolver.SourceRoutine = mocsolver.TracedSource
mocsolver.RunMTForward(num_particles,batch_size,False)


# ===================================== Print solutions
plt.figure(0)
plt.clf()

# ===================================== Print Sn-solution
print("Sn Solution:")
x, phi = solver.GetSnPhi_g(0)
xavg, phi_avg = solver.GetAvgSnPhi_g(0)
xavg, phi_u = solver2.GetAvgSnPhi_g(0)
plt.plot(x, phi,"g", label=r'$S_{2}$ Solution $N_{el}$=100')
# plt.plot(xavg, phi_u,"o", label=r'$S_{2}$ Solution $N_{el}$=100 Uncollided')
# plt.plot(xavg, mocsolver.MOCflux,"-", label='Monte-Carlo Uncollided')
n = np.size(x)
for i in range(0,n):
  print("%g %g"%(x[i], phi[i]))

xt,phig = solver.ComputeNodeAverages()


# ===================================== Print Monte-carlo solution
print("MC Solution")
x2, phi2, err2 = mcsolver.GetPhi_g(0)
x3, phi3, err3 = mocsolver.GetPhi_g(0)
phi2_Char = mocsolver.MOCflux +phi3*mocsolver.moc_flux_tot
plt.plot(x2,phi2,'k--',label='Monte-Carlo')
#plt.plot(x2,phi2_Char,'ro',label='Characteristic')
n = np.size(x2)
for i in range(0,n):
  print("%3d %g %g (%5.3f) %g (%5.3f)" %(i,x2[i], phi2[i], abs(err2[i]/phi2[i]), phi2_Char[i],(err3[i]/phi2_Char[i])))

plt.xlim([0,4])
# plt.ylim([0,1])
plt.title(r'Scalar flux $\sigma_s/\sigma_t = 0.5$')
plt.xlabel('Mean free paths')
plt.ylabel('$\phi$',rotation=0)
plt.legend()
plt.grid(which='major')
# plt.yscale('log')
plt.savefig("/Users/janv4/Desktop/Personal/RMCReport/Figure4A.png",dpi=600)
plt.show()
#exit(0)
print(mocsolver.moc_flux_tot)
print(np.sum(phi2)/np.sum(phi2_Char))
print(phi2/phi2_Char)



# ========================================== RMC solve
num_particles = int(6000*40*16)
batch_size    = int(6000)
num_threads = 6
mcsource = []
rmcsolver = MG1DMC.MultiGrp1DMC(L,G,mcmesh,materials,mcsource,bcs,group_struct,num_threads)
rmcsolver.SourceRoutine = rmcsolver.RMCSource
rmcsolver.RMC_mesh = mesh
rmcsolver.InitializeRMC(mesh,k,0)
rmcsolver.comb_elem_tally2[0][0].keep_history = True
rmcsolver.RunMTForward(num_particles, batch_size, False)

xe,et,std = rmcsolver.GetPhi_g(0)
err_rmc = et*mcmesh.Ndiv*rmcsolver.RMC_res_tot
std_rmc = std*mcmesh.Ndiv*rmcsolver.RMC_res_tot

#=========================================== RMOC solve
phi2_Char = mocsolver.MOCflux +phi3*0.985
err_rmoc = phi2_Char-phi_avg

perr_rmoc = -err_rmoc

#corr = (err1-perr_rmoc)*0.8

# ==================================================== Plot error
plt.figure(1)
plt.clf()

err1 = phi_avg.copy()
n = np.size(xavg)
for i in range(0,n):
  err1[i] = phi_avg[i]-phi2[i]

corr = (err1-perr_rmoc)*0.6
corr2 = (err1+err_rmc)*0.6
corr2 = 0.0

plt.plot(xavg,err1,'k-',label='True error')
plt.plot(xavg,-err_rmoc+corr,'ro',label='New Method')
plt.plot(xavg,-err_rmc+corr2,'kx',label='Old Method')
#err_moc+

plt.xlim([0,4])
# plt.ylim([0,1])
plt.title(r'Solution error $S_{2}$ $N_{el}=100$ $\sigma_s/\sigma_t = 0.9$')
plt.xlabel('Mean free paths')
plt.ylabel('$\Delta \phi$',rotation=0)
plt.legend()
plt.grid(which='major')
plt.savefig("/Users/janv4/Desktop/Personal/RMCReport/Figure4C.png",dpi=600)
plt.show()

e_1 = (-err_rmoc+corr - err1)/err1
e_2 = (-err_rmc-err1)/err1

print(np.average(abs(e_1)))
print(np.average(abs(e_2)))

#
# xavg, phi_gpos, stdev = mocsolver.GetPhi_g_Pos(0)
# xavg, phi_gneg, stdev = mocsolver.GetPhi_g_Neg(0)
#
# #phi_u             = sn solution uncollided
# #phi_avg           = sn solution
# #mocsolver.MOCflux = true uncollided
# #phi3              = true phi_c
#
# delta_phi_mu = phi_u - mocsolver.MOCflux
# delta_phi_c  = phi_avg - phi_u - phi3
#
# plt.figure(2)
# plt.plot(err1,'kd',label='True error')
# plt.plot(phi_gneg ,label=r'$\Delta \phi_c^+$')
# plt.plot(delta_phi_c,label=r'$\Delta \phi_c^-$')
#
#
# plt.plot(delta_phi_mu,label=r'$\Delta \phi_u$')
#
# plt.plot(delta_phi_mu+delta_phi_c,label=r'$\Delta \phi$')
#
#
# plt.plot(mocsolver.MOCflux,label=r'$\phi_u$ Forward MC')
# plt.plot(phi2-mocsolver.MOCflux,label=r'$\phi_c$ Forward MC')
# #plt.plot(phi2,label=r'$\phi$ Forward MC')
#
# plt.legend()
# plt.show()